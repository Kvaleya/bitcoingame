﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BitcoinGame
{
	public class FileFuncProvider : IFuncProvider
	{
		public class FileFunc
		{
			public string Hash;
			public string TeX;
			public List<double> Results = new List<double>();
			public int Stars;
		}

		readonly string _filename;

		public FileFuncProvider(string filename)
		{
			_filename = filename;
		}

		public IEnumerable<MinableFunc> GetFunctions()
		{
			var funcs = new List<FileFunc>();

			double parse(string s)
			{
				return double.Parse(s, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
			}

			using(var sr = new StreamReader(_filename))
			{
				sr.ReadLine();

				while(!sr.EndOfStream)
				{
					var line = sr.ReadLine();

					if (line == null)
						break;

					var split = line.Split(';');

					var f = new FileFunc();

					f.Hash = split[0];

					f.TeX = split[1];

					var sols = split[2].Split(',');

					for(int i = 0; i < sols.Length; i++)
					{
						f.Results.Add(parse(sols[i]));
					}

					f.Stars = Int32.Parse(split[3]);

					funcs.Add(f);
				}
			}

			var minable = new List<MinableFunc>();

			for(int i = 0; i < funcs.Count; i++)
			{
				var f = funcs[i];

				minable.Add(new MinableFunc()
				{
					Stars = f.Stars,
					TeX = f.TeX,
					Solutions = ImmutableArray.Create(f.Results.ToArray()),
					Id= i.ToString(),
					MainImagePath = "images/" + f.Hash + ".png",
				});
			}

			return minable;
		}
	}
}
