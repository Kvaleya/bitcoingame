﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Immutable;

namespace BitcoinGame
{
	// All game state is immutable.

	public enum SoundType
	{
		NewFunc, // TODO
	}

	public record GameState
	{
		public GameConfig Config { get; init; } = new GameConfig();

		/// <summary>
		/// All teams participating in the game. Map of team id -> team.
		/// </summary>
		public ImmutableDictionary<string, Team> Teams { get; init; } = ImmutableDictionary<string, Team>.Empty;

		/// <summary>
		/// All functions that have not been published or solved yet.
		/// </summary>
		public ImmutableArray<MinableFunc> InactiveFunctions { get; init; } = ImmutableArray<MinableFunc>.Empty;

		/// <summary>
		/// Functions that are published for the teams to see, but not solved yet.
		/// </summary>
		public ImmutableDictionary<string, MinableFunc> PublishedFunctions { get; init; } = ImmutableDictionary<string, MinableFunc>.Empty;

		public ImmutableArray<MinableFunc> SolvedFunctions { get; init; } = ImmutableArray<MinableFunc>.Empty;

		public double BitcoinPriceGlobal { get; init; } = 1;

		public int TotalFunctionsSolved { get; init; } = 0;

		public int TotalBtcPresent { get; init; } = 0;

		public int TotalBtcActions { get; init; } = 0;

		public double TotalDollarsPresent { get; init; } = 0;
	}

	/// <summary>
	/// Server-side only function object.
	/// </summary>
	public record MinableFunc
	{
		public string DisplayName { get; init; } = "(func)";
		public string? MainImagePath { get; init; } = null;
		public string? HintImagePath { get; init; } = null;
		public string? TeX { get; init; } = null;
		public int BitcoinReward { get; init; } = 1;

		public string Id { get; init; } = "";
		public ImmutableArray<double> Solutions { get; init; } = ImmutableArray<double>.Empty;
		public int Stars { get; init; } = 1;

		public DateTimeOffset? TimePublished { get; init; } = null;
		public DateTimeOffset? TimeSolved { get; init; } = null;
	}

	/// <summary>
	/// Function object that is sent to clients.
	/// </summary>
	public record FuncDesc
	{
		public string Id { get; init; }
		public string DisplayName { get; init; }
		public string? MainImagePath { get; init; }
		public string? TeX { get; init; }
		public int Stars { get; init; }

		/// <summary>
		/// Only send the hint if the team has purchased the hint powerup (if there even is one in the final game).
		/// </summary>
		public string? HintImagePath { get; init; } = null;
		public int BitcoinReward { get; init; }

		/// <summary>
		/// How many times has the team attempted to solve this function already.
		/// </summary>
		public int AttemptsDone { get; init; } = 0;

		public int AttemptsLeft { get; init; } = 1;

		public double[] Solutions { get; init; } = new double[0];
	}

	public record Team
	{
		/// <summary>
		/// Also doubles as the team's password for the web app.
		/// </summary>
		public string Id { get; init; }
		public string DisplayName { get; init; }
		public int Bitcoins { get; init; } = 0;
		public double Dollars { get; init; } = 0;
		//public ImmutableArray<string> PowerUps { get; init; } = ImmutableArray<string>.Empty;
		public ImmutableArray<MinableFunc> MinedFunctions { get; init; } = ImmutableArray<MinableFunc>.Empty;
		public int TotalSubmits { get; init; } = 0;

		public bool IsAdmin { get; init; } = false;

		public bool AllowHints { get; init; } = false;

		/// <summary>
		/// How many times has a team attempted to solve a function. Map of function id -> number of attempts.
		/// </summary>
		public ImmutableDictionary<string, int> FunctionSolutionAttempts { get; init; } = ImmutableDictionary<string, int>.Empty;

		/// <summary>
		/// Multiplier of the price of bitcoin in dollars.
		/// Values lower than one hurt the team (less dollars per bitcoin sold), values above one help it.
		/// </summary>
		public double LocalBtcToDollarMultiplier { get; init; } = 1;
	}

	public record GameQueueItem
	{
		public ClientRequestWrapper? ClientRequestEvent { get; init; } = null;

		public DisconnectionEvent? DisconnectionEvent { get; init; } = null;

		/// <summary>
		/// Used to enable/disable connection management and data sending.
		/// Used when restoring game state from log file.
		/// </summary>
		public bool? NetworkEnabled { get; init; } = null;

		public object? ExitMainLoopEvent { get; init; } = null;
	}

	public record ConnectionManager
	{
		public const string NullTeam = "$pectator$";

		private ImmutableDictionary<string, string> _connectionIdToTeam { get; init; } = ImmutableDictionary<string, string>.Empty;
		private ImmutableDictionary<string, ImmutableArray<string>> _teamToConnectionIds { get; init; } = ImmutableDictionary<string, ImmutableArray<string>>.Empty;

		public ImmutableArray<string> GetTeamConnections(string team)
		{
			if (_teamToConnectionIds.ContainsKey(team))
				return _teamToConnectionIds[team];
			return ImmutableArray<string>.Empty;
		}

		public IEnumerable<string> AllConnections
		{
			get
			{
				return _connectionIdToTeam.Keys;
			}
		}

		public ConnectionManager WithConnectionPresent(string connectionId, string? team)
		{
			var ret = this;

			if (string.IsNullOrEmpty(team))
				team = NullTeam;

			if (_connectionIdToTeam.ContainsKey(connectionId))
			{
				if(_connectionIdToTeam[connectionId] == team)
				{
					// Nothing to change
					return this;
				}
				else
				{
					// Clear the previous connection
					ret = ret.WithConnectionRemoved(connectionId);
				}
			}

			var newDict = ret._teamToConnectionIds;

			if(!newDict.ContainsKey(team))
			{
				newDict = newDict.Add(team, ImmutableArray.Create(connectionId));
			}
			else
			{
				newDict = newDict.SetItem(team, newDict[team].Add(connectionId));
			}

			return ret with
			{
				_connectionIdToTeam = ret._connectionIdToTeam.Add(connectionId, team),
				_teamToConnectionIds = newDict,
			};
		}

		public ConnectionManager WithConnectionRemoved(string connectionId)
		{
			if (!_connectionIdToTeam.ContainsKey(connectionId))
				return this;

			var team = this._connectionIdToTeam[connectionId];

			return this with
			{
				_connectionIdToTeam = this._connectionIdToTeam.Remove(connectionId),
				_teamToConnectionIds = this._teamToConnectionIds.SetItem(team, this._teamToConnectionIds[team].Remove(connectionId)),
			};
		}
	}

	public record DisconnectionEvent
	{
		public string ConnectionId { get; init; } = "";
	}

	public record ClientRequestWrapper
	{
		public string ConnectionId { get; init; } = "";
		public ClientRequest Request { get; init; } = new ClientRequest();
	}

	public record ClientRequest
	{
		public string? SubmitterTeamId { get; init; } = null;

		// User stuff
		public InputSubmitAttempt? Submit { get; init; } = null;
		public InputSellBitcoin? SellBitcoin { get; init; } = null;

		// Admin stuff
		public InputSetTeamDollarMult? SetTeamDollarMult { get; init; } = null;
		public InputPurchasePowerup? Purchase { get; init; } = null;
		public InputSetProperty? SetProperty { get; init; } = null;
		public InputSkipFuncs? SkipFuncs { get; init; } = null;
	}

	public record InputSkipFuncs
	{
		public int Count { get; init; }
	}

	public record InputPurchasePowerup
	{
		public string TeamName { get; init; } = "";
		public double Dollars { get; init; } = 0;
	}

	public record InputSubmitAttempt
	{
		public string FuncId { get; init; } = "";
		public double Result { get; init; } = 0;
	}

	public record InputSetTeamDollarMult
	{
		public string TeamName { get; init; } = "";
		public double NewValue { get; init; } = 1;
	}

	public record InputSetProperty
	{
		public string Name { get; init; } = "";
		public string Value { get; init; } = "";
	}

	public record InputSellBitcoin
	{
		public int Amount { get; init; }
	}

	public record ServerResponse
	{
		public FuncDesc[] Functions { get; init; } = new FuncDesc[0];
		public FuncDesc[] LatestSolvedFunctions { get; init; } = new FuncDesc[0];
		public ResponseTeamStats TeamStats { get; init; } = new ResponseTeamStats();
		public ResponseOtherTeamStats[] OtherTeams { get; init; } = new ResponseOtherTeamStats[0];

		public ResponseSolutionAttempt? AttemptResponse { get; init; } = null;
		public ResponseBitcoinSold? BitcoinsSoldResponse { get; init; } = null;
		public string? SetPropertyFeedback { get; init; } = null;
		public bool IsAdmin { get; init; } = false;
	}

	public record ResponseSolutionAttempt
	{
		public bool Accepted { get; init; }
		public int AttemptsLeft { get; init; }
	}

	public record ResponseBitcoinSold
	{
		public int BitcoinsSold { get; init; }
		public double DollarsAcquired { get; init; }
	}

	public record ResponseTeamStats
	{
		public double BitcoinPrice { get; init; } = 0;
		public int Bitcoins { get; init; } = 0;
		public double Dollars { get; init; } = 0;
		public string Name { get; init; } = "";
		public bool Valid { get; init; } = false;
		public int TotalSolved { get; init; } = 0;
		public int TotalSubmits { get; init; } = 0;
	}

	public record ResponseOtherTeamStats
	{
		public int Bitcoins { get; init; } = 0;
		public string Name { get; init; } = "";
		public int TotalSubmits { get; init; } = 0;
		public double Dollars { get; init; } = 0;
		public double Mult { get; init; } = 1;
	}
}
