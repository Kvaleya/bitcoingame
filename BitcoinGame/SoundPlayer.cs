﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Net.WebSockets;
using Microsoft.Extensions.Logging;

namespace BitcoinGame
{
	public class SoundPlayer
	{
		// TODO: implement sound interface
		readonly ILogger<SoundPlayer> _logger;

		public SoundPlayer(ILogger<SoundPlayer> logger)
		{
			_logger = logger;
		}

		public void PlaySound(GameState state, SoundType type)
		{
			// MUST NOT CRASH
			// MUST NOT BLOCK

			// socket.send('MPD_API_PLAY_TRACK,' + $(this).attr('trackid'));

			Task.Run(() =>
			{
				try
				{
					ClientWebSocket socket = new ClientWebSocket();
					socket.ConnectAsync(new Uri("ws://omnian.lan:8080/"), CancellationToken.None).Wait();
					socket.SendAsync(GetBytes($"MPD_API_PLAY_TRACK,{state.Config.FunctionMinedSoundId}"), WebSocketMessageType.Text, true, CancellationToken.None);
				}
				catch(Exception e)
				{
					// :(
					_logger.LogError(e, "Sound player failed!");
				}
			});
		}

		static byte[] GetBytes(string str)
		{
			byte[] bytes = new byte[str.Length];
			for(int i = 0; i < str.Length; i++)
			{
				bytes[i] = (byte)str[i];
			}
			return bytes;
		}
	}
}
