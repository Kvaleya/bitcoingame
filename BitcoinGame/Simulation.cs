﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;

namespace BitcoinGame
{
	static class Simulation
	{
		// TODO: game actions that manipulate main state

		/// <summary>
		/// Gets bitcoin price in dollars for a given team, or returns zero if the team is invalid.
		/// </summary>
		/// <param name="state"></param>
		/// <param name="teamId"></param>
		/// <returns></returns>
		static double GetBitcoinPrice(GameState state, string? teamId)
		{
			var t = GetTeam(state, teamId);
			if (t == null)
				return 0;
			var price = state.BitcoinPriceGlobal * t.LocalBtcToDollarMultiplier;
			price *= Math.Pow(state.Config.BtcPriceBitcoinTeamPresenceFactor, t.Bitcoins);
			return price;
		}

		/// <summary>
		/// Gets a team instance from state and team id.
		/// Returns null if the team does not exist or teamId is null.
		/// </summary>
		static Team? GetTeam(GameState state, string? teamId)
		{
			if (string.IsNullOrEmpty(teamId) || !state.Teams.ContainsKey(teamId))
				return null;
			return state.Teams[teamId];
		}

		/// <summary>
		/// Tries to find a team by name, returns null if fails.
		/// </summary>
		static Team? FindTeam(GameState state, string? teamName)
		{
			if (string.IsNullOrEmpty(teamName))
				return null;
			var found = state.Teams.Values.Where(x => x.DisplayName.ToLowerInvariant().StartsWith(teamName.ToLowerInvariant())).ToList();
			if (found.Count == 0 || found.Count > 1)
				return null;
			return found[0];
		}

		static FuncDesc FromMinable(GameState state, MinableFunc f, Team? t)
		{
			int attemptsDone = 0;
			int attemptsLeft = state.Config.MaxSolutionSubmitAttempts;

			if (t != null)
			{
				if (t.FunctionSolutionAttempts.ContainsKey(f.Id))
				{
					attemptsDone = t.FunctionSolutionAttempts[f.Id];
				}
				else
				{
					attemptsDone = 0;
				}
			}

			return new FuncDesc()
			{
				TeX = f.TeX,
				BitcoinReward = f.BitcoinReward,
				DisplayName = f.DisplayName,
				HintImagePath = f.HintImagePath,
				MainImagePath = f.MainImagePath,
				Id = f.Id,
				Stars = f.Stars,
				AttemptsDone = attemptsDone,
				AttemptsLeft = attemptsLeft,
				Solutions = (f.TimeSolved != null) ? f.Solutions.ToArray() : new double[0],
			};
		}

		/// <summary>
		/// Returns (functions a given team can see, latest solved functions). Functions are team specific (because of number of attempts).
		/// TeamId may be null, then number of attempts will always be zero.
		/// </summary>
		static (FuncDesc[], FuncDesc[]) GetFuncs(GameState state, string? teamId)
		{
			var funcs = new List<FuncDesc>();
			var solved = new List<FuncDesc>();

			var t = GetTeam(state, teamId);

			foreach (var f in state.PublishedFunctions.Values.OrderBy(x => x.TimePublished))
			{
				funcs.Add(FromMinable(state, f, t));
			}

			for(int i = 0; i < state.Config.VisibleSolvedFunctionCount; i++)
			{
				int index = state.SolvedFunctions.Length - i - 1;
				if (index < 0)
					break;
				solved.Add(FromMinable(state, state.SolvedFunctions[index], t));
			}

			return (funcs.ToArray(), solved.ToArray());
		}

		static ResponseOtherTeamStats[] GetOtherTeamsStats(GameState state, bool admin)
		{
			if (!admin)
				return new ResponseOtherTeamStats[0];

			return state.Teams.Values.Where(x => !x.IsAdmin).Select(x =>
			{
				return new ResponseOtherTeamStats()
				{
					Name = x.DisplayName,
					Bitcoins = x.Bitcoins,
					TotalSubmits = x.TotalSubmits,
					Dollars = admin ? x.Dollars : 0,
					Mult = admin ? x.LocalBtcToDollarMultiplier : 1,
				};
			}).ToArray();
		}

		static ResponseTeamStats GetTeamStats(GameState state, string? teamId)
		{
			var t = GetTeam(state, teamId);

			if (t == null)
			{
				return new ResponseTeamStats()
				{
					Bitcoins = 0,
					Dollars = 0,
					Name = "",
					Valid = false,
				};
			}

			return new ResponseTeamStats()
			{
				Bitcoins = t.Bitcoins,
				Dollars = t.Dollars,
				Name = t.DisplayName,
				Valid = true,
				TotalSolved = t.MinedFunctions.Length,
				TotalSubmits = t.TotalSubmits,
				BitcoinPrice = GetBitcoinPrice(state, teamId),
			};
		}

		static ServerResponse GenResponse(GameState state, string? teamId, ResponseOtherTeamStats[] others)
		{
			(var funcs, var solved) = GetFuncs(state, teamId);

			var t = GetTeam(state, teamId);

			return new ServerResponse()
			{
				Functions = funcs,
				LatestSolvedFunctions = solved,
				TeamStats = GetTeamStats(state, teamId),
				OtherTeams = others,
				IsAdmin = t == null ? false : t.IsAdmin,
			};
		}

		/// <summary>
		/// Sends appropriate data to all relevant connections.
		/// Only the current connectionId gets a ResponseSolutionAttempt.
		/// When updateTeam is true, the entire team is updated.
		/// When updateAll is true, everyone is updated.
		/// </summary>
		static void SendResponses(GameState state, SimulationCore core,
			string connectionId, string? teamId,
			ResponseSolutionAttempt? attemptResponse = null,
			ResponseBitcoinSold? bitcoinsSoldResponse = null,
			string? setPropFdb = null,
			bool updateTeam = false, bool updateAll = false)
		{
			if (updateAll)
			{
				foreach(var t in state.Teams.Values)
				{
					core.SendToTeam(t.Id, GenResponse(state, t.Id, GetOtherTeamsStats(state, t.IsAdmin)), connectionId);
				}
				core.SendToTeam(ConnectionManager.NullTeam, GenResponse(state, null, GetOtherTeamsStats(state, false)), connectionId);
			}
			else
			{
				var t = GetTeam(state, teamId);

				if (updateTeam && t != null)
				{
					core.SendToTeam(t.Id, GenResponse(state, t.Id, GetOtherTeamsStats(state, t.IsAdmin)), connectionId);
				}
			}

			var localTeam = GetTeam(state, teamId);

			var r = GenResponse(state, teamId, GetOtherTeamsStats(state, localTeam?.IsAdmin ?? false));

			if(attemptResponse != null)
			{
				r = r with
				{
					AttemptResponse = attemptResponse,
				};
			}
			if(bitcoinsSoldResponse != null)
			{
				r = r with
				{
					BitcoinsSoldResponse = bitcoinsSoldResponse,
				};
			}

			if(setPropFdb != null)
			{
				r = r with
				{
					SetPropertyFeedback = setPropFdb,
				};
			}

			core.SendToConnection(connectionId, r);
		}

		static bool IsSolutionCorrect(GameState state, double expected, double actual)
		{
			return Math.Abs(expected - actual) < state.Config.MaxResultAttemptDifference;
		}

		static GameState TryToSubmit(GameState state, SimulationCore core, string? teamId, string funcId, double sol, out ResponseSolutionAttempt response)
		{
			response = new ResponseSolutionAttempt()
			{
				Accepted = false,
				AttemptsLeft = -1,
			};

			if (teamId == null || GetTeam(state, teamId) == null)
				return state;

			if (!state.PublishedFunctions.ContainsKey(funcId))
				return state;

			if (state.Teams[teamId].FunctionSolutionAttempts.ContainsKey(funcId)
				&& state.Teams[teamId].FunctionSolutionAttempts[funcId] >= state.Config.MaxSolutionSubmitAttempts)
				return state;

			var f = state.PublishedFunctions[funcId];

			// Add attempt to team
			var attempts = state.Teams[teamId].FunctionSolutionAttempts;

			if (attempts.ContainsKey(funcId))
			{
				attempts = attempts.SetItem(funcId, attempts[funcId] + 1);
			}
			else
			{
				attempts = attempts.Add(funcId, 1);
			}

			state = state with
			{
				Teams = state.Teams.SetItem(teamId, state.Teams[teamId] with
				{
					FunctionSolutionAttempts = attempts,
					TotalSubmits = state.Teams[teamId].TotalSubmits + 1,
				}),
			};

			// Set attempts left
			response = response with
			{
				AttemptsLeft = Math.Max(state.Config.MaxSolutionSubmitAttempts - state.Teams[teamId].FunctionSolutionAttempts[funcId], 0),
			};

			if (state.Teams[teamId].FunctionSolutionAttempts.ContainsKey(funcId))
			{
				if(state.Teams[teamId].FunctionSolutionAttempts[funcId] > state.Config.MaxSolutionSubmitAttempts)
				{
					return state;
				}
			}

			bool correct = false;

			foreach(var s in f.Solutions)
			{
				if(IsSolutionCorrect(state, s, sol))
				{
					correct = true;
					break;
				}
			}

			if(correct)
			{
				response = response with
				{
					Accepted = true,
				};

				var updatedFunc = f with
				{
					TimeSolved = DateTimeOffset.UtcNow,
				};

				state = state with
				{
					Teams = state.Teams.SetItem(teamId, state.Teams[teamId] with
					{
						Bitcoins = state.Teams[teamId].Bitcoins + f.BitcoinReward,
						MinedFunctions = state.Teams[teamId].MinedFunctions.Add(updatedFunc),
					}),
					PublishedFunctions = state.PublishedFunctions.Remove(f.Id),
					SolvedFunctions = state.SolvedFunctions.Add(updatedFunc),
					TotalBtcPresent = state.TotalBtcPresent + f.BitcoinReward,
					TotalFunctionsSolved = state.TotalFunctionsSolved + 1,
					TotalBtcActions = state.TotalBtcActions + 1,
				};

				core.PlaySound(SoundType.NewFunc);

				return state;
			}
			else
			{
				return state;
			}
		}

		static GameState SellBtc(GameState state, string? teamId, InputSellBitcoin sell, out ResponseBitcoinSold btcSoldResponse)
		{
			btcSoldResponse = new ResponseBitcoinSold()
			{
				BitcoinsSold = 0,
				DollarsAcquired = 0,
			};

			var t = GetTeam(state, teamId);

			if (t == null || teamId == null)
				return state;

			if(t.Bitcoins < sell.Amount || sell.Amount < 0)
			{
				return state;
			}

			double newDollars = sell.Amount * GetBitcoinPrice(state, teamId);

			t = t with
			{
				Bitcoins = t.Bitcoins - sell.Amount,
				Dollars = t.Dollars + newDollars,
			};

			btcSoldResponse = btcSoldResponse with
			{
				BitcoinsSold = sell.Amount,
				DollarsAcquired = newDollars,
			};

			state = state with
			{
				Teams = state.Teams.SetItem(teamId, t),
				TotalBtcPresent = state.TotalBtcPresent - sell.Amount,
				TotalDollarsPresent = state.TotalDollarsPresent + newDollars,
				TotalBtcActions = state.TotalBtcActions + (sell.Amount != 0 ? 1 : 0),
			};

			return state;
		}

		static uint Random(int seed)
		{
			unchecked
			{
				uint x = (uint)seed;
				x = ((x >> 16) ^ x) * 0x45d9f3b;
				x = ((x >> 16) ^ x) * 0x45d9f3b;
				x = (x >> 16) ^ x;
				return x;
			}
		}

		/// <summary>
		/// Returns pseudo-random value in range -1..1 entirely determined by the seed.
		/// </summary>
		static double RandomSignedNormalized(int seed)
		{
			return (Random(seed) / (double)uint.MaxValue) * 2.0 - 1.0;
		}

		static GameState ProcessAdminStuff(GameState state, ClientRequest request, out bool updateAll, out string? setPropFbd)
		{
			setPropFbd = null;
			updateAll = false;
			var orgTeam = GetTeam(state, request.SubmitterTeamId);
			if (orgTeam == null || !orgTeam.IsAdmin)
			{
				return state;
			}

			if (request.SetTeamDollarMult != null)
			{
				var t = FindTeam(state, request.SetTeamDollarMult.TeamName);

				if (t == null)
				{
					setPropFbd = "Tým nenalezen :(";
					return state;
				}

				if (t != null)
				{
					state = state with
					{
						Teams = state.Teams.SetItem(t.Id, state.Teams[t.Id] with
						{
							LocalBtcToDollarMultiplier = request.SetTeamDollarMult.NewValue,
						}),
					};
					updateAll = true;
				}
			}

			if(request.Purchase != null)
			{
				var t = FindTeam(state, request.Purchase.TeamName);

				if (t == null)
				{
					setPropFbd = "Tým nenalezen :(";
					return state;
				}

				if (t != null && t.Dollars >= request.Purchase.Dollars)
				{
					updateAll = true;
					state = state with
					{
						Teams = state.Teams.SetItem(t.Id, state.Teams[t.Id] with
						{
							Dollars = t.Dollars - request.Purchase.Dollars,
						}),
					};
				}
			}

			if(request.SetProperty != null)
			{
				TrySetProperty(state.Config, request.SetProperty.Name, request.SetProperty.Value, out var old, out var setnew);

				if(setnew)
				{
					updateAll = true;
					setPropFbd = $"Property {request.SetProperty.Name} set from {old} to {request.SetProperty.Value}!";
				}
				else
				{
					setPropFbd = $"Property setting failed!";
				}
			}

			if(request.SkipFuncs != null && request.SkipFuncs.Count > 0)
			{
				updateAll = true;
				int skipped = 0;
				int i = request.SkipFuncs.Count;
				while(i > 0)
				{
					i--;

					if (state.InactiveFunctions.Length == 0)
						break;

					state = state with
					{
						InactiveFunctions = state.InactiveFunctions.RemoveAt(0),
					};

					skipped++;
				}

				setPropFbd = $"Skipped {skipped} unpublished functions. Remains: {state.InactiveFunctions.Length}.";
			}

			return state;
		}

		public static GameState StateWithUpdateBtcPrice(GameState state)
		{
			double sgnSqr(double d)
			{
				return (d * d) * Math.Sign(d); // Sign preserving square
			}

			double price = state.Config.BaseBitcoinPrice;
			price *= Math.Pow(state.Config.BtcPriceTotalMinedFactor, state.TotalFunctionsSolved);
			price *= Math.Pow(state.Config.BtcPriceBitcoinPresenceFactor, state.TotalBtcPresent);
			price *= Math.Pow(state.Config.BtcPriceDollarPresenceFactor, state.TotalDollarsPresent);
			double rnd = RandomSignedNormalized(state.TotalBtcActions);

			// Square the mostly uniform random value to make peaks rarer and values close to zero more common.
			price *= 1.0 + sgnSqr(rnd) * state.Config.BtcPriceMaxDeviation;

			state = state with
			{
				BitcoinPriceGlobal = price,
			};

			return state;
		}

		public static GameState Process(ClientRequest request, string connectionId, SimulationCore core, GameState state)
		{
			ResponseSolutionAttempt? solutionResponse = null;
			ResponseBitcoinSold? bitcoinsSold = null;

			bool updateTeam = false;
			bool updateAll = false;

			// Handle function solution submit
			if(request.Submit != null)
			{
				state = TryToSubmit(state, core, request.SubmitterTeamId, request.Submit.FuncId, request.Submit.Result, out solutionResponse);
				updateTeam = true;
				updateAll = true;
			}

			// Handle bitcoin selling
			if(request.SellBitcoin != null)
			{
				state = SellBtc(state, request.SubmitterTeamId, request.SellBitcoin, out bitcoinsSold);
				updateAll = true;
				updateTeam = true;
			}

			state = ProcessAdminStuff(state, request, out var adminUpdate, out var setPropFdb);
			updateAll |= adminUpdate;

			// Fill in functions
			state = StateWithFilledFunctions(state);

			// Update btc price
			state = StateWithUpdateBtcPrice(state);

			// Send responses
			SendResponses(state, core, connectionId, request.SubmitterTeamId,
				attemptResponse: solutionResponse,
				bitcoinsSoldResponse: bitcoinsSold,
				setPropFdb: setPropFdb,
				updateTeam: updateTeam,
				updateAll: updateAll);

			// Return new state
			return state;
		}

		public static GameState StateWithFilledFunctions(GameState state)
		{
			while(state.PublishedFunctions.Count < state.Config.AvailableFunctionCount && state.InactiveFunctions.Length > 0)
			{
				var f = state.InactiveFunctions[0];

				state = state with
				{
					InactiveFunctions = state.InactiveFunctions.RemoveAt(0),
					PublishedFunctions = state.PublishedFunctions.Add(f.Id, f with
					{
						TimePublished = DateTimeOffset.UtcNow,
					}),
				};
			}

			return state;
		}

		public static GameState StateWithTeamsCreated(GameState state)
		{
			List<Team> teams = new List<Team>()
			{
				new Team()
				{
					DisplayName = "Alpha",
					Id = "algebra",
				},
				new Team()
				{
					DisplayName = "Bravo",
					Id = "binary",
				},
				new Team()
				{
					DisplayName = "Charlie",
					Id = "colorscience",
				},
				new Team()
				{
					DisplayName = "Delta",
					Id = "diskretka",
				},
				new Team()
				{
					DisplayName = "Echo",
					Id = "epsilon",
				},
				new Team()
				{
					DisplayName = "Foxtrot",
					Id = "funkce",
				},
				//new Team()
				//{
				//	DisplayName = "Golf",
				//	Id = "geometrie",
				//},
				new Team()
				{
					DisplayName = "Orgstvo",
					Id = "mlcetikocka",
					IsAdmin = true,
				}
			};

			return state with
			{
				Teams = System.Collections.Immutable.ImmutableDictionary.CreateRange(teams.Select(x => new KeyValuePair<string, Team>(x.Id, x))),
			};
		}

		/// <summary>
		/// Returns old value.
		/// </summary>
		static void TrySetProperty(GameConfig constants, string name, string? value, out string? oldValue, out bool setNew)
		{
			oldValue = null;
			setNew = false;

			System.Reflection.PropertyInfo? prop = typeof(GameConfig).GetProperties().SingleOrDefault(x => x.Name.ToLowerInvariant() == name.ToLowerInvariant());

			object obj = constants;

			if (prop == null)
			{
				return;
			}

			oldValue = prop.GetValue(obj)?.ToString();

			if (value != null)
			{
				if (prop.PropertyType == typeof(bool))
				{
					bool target = true;

					if (value == "False" || value == "false" || value == "f" || value == "0")
					{
						target = false;
					}

					prop.SetValue(obj, target);
					setNew = true;
					return;
				}

				if (prop.PropertyType == typeof(int))
				{
					if (int.TryParse(value, out var i))
					{
						prop.SetValue(obj, i);
						setNew = true;
						return;
					}
				}

				if (prop.PropertyType == typeof(double))
				{
					if (double.TryParse(value, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out var d))
					{
						prop.SetValue(obj, d);
						setNew = true;
						return;
					}
				}
			}
		}
	}
}
