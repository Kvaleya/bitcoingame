using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace BitcoinGame
{
	/// <summary>
	/// Single hub used for everything
	/// </summary>
	public class CommonHub : Hub
	{
		private readonly ILogger<CommonHub> _logger;
		private readonly MainLoop _loop;

		public CommonHub(ILogger<CommonHub> logger, MainLoop mainLoop)
		{
			_logger = logger;
			_loop = mainLoop;
		}

		public void OnClientRequest(ClientRequest request)
		{
			_loop.Enqueue(new GameQueueItem()
			{
				ClientRequestEvent = new ClientRequestWrapper()
				{
					ConnectionId = Context.ConnectionId,
					Request = request ?? new ClientRequest(),
				},
			});
		}

		/// <summary>
		/// Generated only when a client purposefully terminates the connection.
		/// </summary>
		public override Task OnDisconnectedAsync(Exception exception)
		{
			_loop.Enqueue(new GameQueueItem()
			{
				DisconnectionEvent = new DisconnectionEvent()
				{
					ConnectionId = Context.ConnectionId,
				},
			});

			return base.OnDisconnectedAsync(exception);
		}
	}
}
