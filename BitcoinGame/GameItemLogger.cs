﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BitcoinGame
{
	public class GameItemLogger : IDisposable
	{
		static readonly JsonSerializerOptions Options = new JsonSerializerOptions()
		{
			WriteIndented = false,
		};

		string _filename;
		StreamWriter _writer;

		public readonly string LastestFileName = null;

		public GameItemLogger(string outputFile = null)
		{
			if(string.IsNullOrEmpty(outputFile))
			{
				var now = DateTimeOffset.UtcNow;
				outputFile = $"record_{now.Year.ToString("D4")}_{now.Month.ToString("D2")}_{now.Day.ToString("D2")}-_{now.Hour.ToString("D2")}_{now.Minute.ToString("D2")}_{now.Second.ToString("D2")}-{now.Millisecond.ToString("D3")}.game";
				outputFile = Path.Combine(GameConfig.GameLogPath, outputFile);
			}

			_filename = outputFile;

			if(File.Exists(_filename))
			{
				throw new Exception($"File of that name already exists! {_filename}");
			}

			Directory.CreateDirectory(_filename.Substring(0, _filename.Length - Path.GetFileName(_filename).Length));

			LastestFileName = GetLatestLogFullPath();

			_writer = new StreamWriter(File.Open(_filename, FileMode.Create, FileAccess.Write), Encoding.UTF8);
		}

		public void Store(GameQueueItem item)
		{
			_writer.WriteLine(JsonSerializer.Serialize(item, Options));
			_writer.Flush();
			_writer.BaseStream.Flush();
		}

		public void Dispose()
		{
			_writer.Dispose();
		}

		public static void DoAutoRestore(MainLoop loop, GameItemLogger gil)
		{
			if(gil.LastestFileName != null)
			{
				loop.Restore(Deserialize(gil.LastestFileName));
			}
		}

		static string GetLatestLogFullPath()
		{
			if (Directory.GetFiles(GameConfig.GameLogPath).Length < 1)
				return null;

			var latest = Directory.GetFiles(GameConfig.GameLogPath).Where(s => new FileInfo(s).Length > 0).OrderByDescending(s => File.GetCreationTime(s)).First();
			return latest;
		}

		public static IEnumerable<GameQueueItem> Deserialize(string file)
		{
			List<GameQueueItem> items = new List<GameQueueItem>();

			using(StreamReader sr = new StreamReader(file))
			{
				while(!sr.EndOfStream)
				{
					var line = sr.ReadLine();

					if (line == null)
						break;

					var item = JsonSerializer.Deserialize<GameQueueItem>(line, Options);
					items.Add(item);
				}
			}

			return items;
		}
	}
}
