﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BitcoinGame
{
	public interface IFuncProvider
	{
		public IEnumerable<MinableFunc> GetFunctions();
	}
}
