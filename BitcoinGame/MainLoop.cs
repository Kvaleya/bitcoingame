using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace BitcoinGame
{
	public class MainLoop
	{
		Thread _loopThread;
		BlockingCollection<GameQueueItem> _queue = new BlockingCollection<GameQueueItem>();
		SimulationCore _sim;
		ILogger<MainLoop> _logger;

		public MainLoop(ILogger<MainLoop> logger, SimulationCore sim, GameItemLogger gil)
		{
			_logger = logger;
			_sim = sim;
			Start();

			if (GameConfig.AutoRestore)
			{
				GameItemLogger.DoAutoRestore(this, gil);
			}
		}

		void Start()
		{
			if (_loopThread != null)
				throw new Exception("Main loop is already started!");

			_logger.LogInformation("Starting main loop");
			_loopThread = new Thread(() => this.Loop());
			_loopThread.Start();
		}

		void Loop()
		{
			while (true)
			{
				var item = _queue.Take();

				if (item.ExitMainLoopEvent != null)
				{
					_logger.LogInformation("Exiting main loop");
					return;
				}

				_sim.Process(item);
			}
		}

		public void Enqueue(GameQueueItem item)
		{
			EnqueueInternal(item);
		}

		void EnqueueInternal(GameQueueItem item)
		{
			if (item == null)
			{
				throw new ArgumentNullException(nameof(item), "Queue items cannot be null!");
			}

			_queue.Add(item);
		}

		public void Restore(IEnumerable<GameQueueItem> items)
		{
			EnqueueInternal(new GameQueueItem()
			{
				NetworkEnabled = false,
			});

			foreach (var i in items)
			{
				EnqueueInternal(i);
			}

			EnqueueInternal(new GameQueueItem()
			{
				NetworkEnabled = true,
			});
		}
	}
}
