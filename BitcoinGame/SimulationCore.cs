﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;

using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace BitcoinGame
{
	public class SimulationCore
	{
		// Note: all data is private. Simulation itself is static, and only uses SimulationCore to send stuff.
		// State is only updates AFTER all actions are done, making sure it never gets into an invalid state due to exceptions.

		public const string ClientRecieveMethod = "OnServerResponse";
		const string DefaultPlayerName = "unnamed";

		private readonly IHubContext<CommonHub> _hubContext;
		private readonly ILogger<SimulationCore> _logger;
		private readonly GameItemLogger _gameLogger;
		private readonly SoundPlayer _soundPlayer;
		private readonly IFuncProvider _funcProvider;

		private readonly List<(string, object)> _sendQueue = new List<(string, object)>();
		private readonly List<SoundType> _soundQueue = new List<SoundType>();

		GameState _mainState = new GameState();
		ConnectionManager _connectionManager = new ConnectionManager();
		private bool _networkEnabled = true;

		public SimulationCore(ILogger<SimulationCore> logger, IHubContext<CommonHub> hubContext,
			GameItemLogger gameLogger, SoundPlayer soundPlayer,
			IFuncProvider funcProvider)
		{
			_logger = logger;
			_hubContext = hubContext;
			_gameLogger = gameLogger;
			_soundPlayer = soundPlayer;
			_funcProvider = funcProvider;

			InitState();
		}

		void InitState()
		{
			var funcs = _funcProvider.GetFunctions().ToArray();

			_mainState = _mainState with
			{
				InactiveFunctions = ImmutableArray.Create(funcs)
			};
			_mainState = Simulation.StateWithUpdateBtcPrice(_mainState);
			_mainState = Simulation.StateWithTeamsCreated(_mainState);
		}

		public void SendToConnection(string connectionId, object payload)
		{
			_sendQueue.Add((connectionId, payload));
		}

		public void SendToTeam(string teamId, object payload, string? excludeConnectionId = null)
		{
			foreach (var connectionId in _connectionManager.GetTeamConnections(teamId))
			{
				if (connectionId != excludeConnectionId)
				{
					SendToConnection(connectionId, payload);
				}
			}
		}

		public void SendToAll(object payload, string? excludeConnectionId = null)
		{
			foreach (var connectionId in _connectionManager.AllConnections)
			{
				if (connectionId != excludeConnectionId)
				{
					SendToConnection(connectionId, payload);
				}
			}
		}

		public void PlaySound(SoundType type)
		{
			_soundQueue.Add(type);
		}

		public void PlaySoundInternal(SoundType type)
		{
			if (!GameConfig.AllowSounds || !_networkEnabled)
				return;
			_soundPlayer.PlaySound(_mainState, type);
		}

		void SendInternal(string connectionId, object payload)
		{
			if (!_networkEnabled)
				return;
			string json = JsonSerializer.Serialize(payload, new JsonSerializerOptions()
			{
				WriteIndented = false,
			});
			_hubContext.Clients.Client(connectionId).SendAsync(ClientRecieveMethod, json);
		}

		void ProcessAndClearQueues()
		{
			foreach((var connectionId, var payload) in _sendQueue)
			{
				SendInternal(connectionId, payload);
			}
			foreach (var s in _soundQueue)
			{
				PlaySoundInternal(s);
			}
			ClearQueues();
		}

		void ClearQueues()
		{
			_sendQueue.Clear();
			_soundQueue.Clear();
		}

		public void Process(GameQueueItem item)
		{
			if(item.NetworkEnabled != null)
			{
				_networkEnabled = item.NetworkEnabled.Value;
			}

			var oldState = _mainState;

			if(GameConfig.RecoverFromExceptions)
			{
				try
				{
					ProcessMain(item);
				}
				catch (Exception e)
				{
					_logger.LogError(e.ToString());
					// Restore game state to pre-exception
					_mainState = oldState;
					ClearQueues();
				}
			}
			else
			{
				ProcessMain(item);
			}

			// Only send stuff after successfully simulating the game
			ProcessAndClearQueues();

			// Only after successful processing of the game item will it be logged
			var logItem = item;

			// First clear network info
			logItem = logItem with
			{
				NetworkEnabled = null,
			};

			// Store
			_gameLogger.Store(logItem);
		}

		void ProcessMain(GameQueueItem item)
		{
			if (item.DisconnectionEvent != null)
			{
				OnDisconnect(item.DisconnectionEvent);
			}

			if (item.ClientRequestEvent != null)
			{
				OnClientRequest(item.ClientRequestEvent.Request, item.ClientRequestEvent.ConnectionId);
			}
		}

		void OnDisconnect(DisconnectionEvent e)
		{
			if (!_networkEnabled)
				return;
			_connectionManager = _connectionManager.WithConnectionRemoved(e.ConnectionId);
		}

		void OnClientRequest(ClientRequest request, string connectionId)
		{
			// Clear team id in case it is garbage
			if(string.IsNullOrEmpty(request.SubmitterTeamId) || !_mainState.Teams.ContainsKey(request.SubmitterTeamId))
			{
				request = request with
				{
					SubmitterTeamId = null,
				};
			}

			if (_networkEnabled)
			{
				// First, register the connection.
				_connectionManager = _connectionManager.WithConnectionPresent(connectionId, request.SubmitterTeamId);
			}

			_mainState = Simulation.Process(request, connectionId, this, _mainState);
		}
	}
}
