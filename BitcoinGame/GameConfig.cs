﻿using System;
using System.Collections.Immutable;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BitcoinGame
{
	public record GameConfig
	{
		public const bool AutoRestore = true; // TODO: set this to true for live game (to restore from crashes)
		public const bool RecoverFromExceptions = true; // TODO: this should be true
		public const bool AllowSounds = true; // TODO: this should be true for live game

		public const string GameLogPath = "../../BitcoinGameLogs";

		public int FunctionMinedSoundId { get; init; } = 33;

		public int AvailableFunctionCount { get; init; } = 0;

		public int VisibleSolvedFunctionCount { get; init; } = 5;

		public double BaseBitcoinPrice { get; init; } = 1;

		/// <summary>
		/// How much can the bitcoin price change randomly.
		/// Price *= 1.0 + (random value from -1..1) * (this value)
		/// </summary>
		public double BtcPriceMaxDeviation { get; init; } = 0.1;

		/// <summary>
		/// How much does bitcoin price increase with each function solved.
		/// Price *= pow(this value, total bitcoins mined)
		/// </summary>
		public double BtcPriceTotalMinedFactor { get; init; } = 1.008; // expected increase: 10x

		/// <summary>
		/// How much does btc price change with the amount of bitcoins the teams hold collectively.
		/// Price *= pow(this value, total bitcoins held)
		/// </summary>
		public double BtcPriceBitcoinPresenceFactor { get; init; } = 1.0;

		/// <summary>
		/// How much does btc price change with the amount of dollars the teams hold collectively.
		/// Price *= pow(this value, total dollars held)
		/// </summary>
		public double BtcPriceDollarPresenceFactor { get; init; } = 0.995; // expected decrease: 3x

		public double BtcPriceBitcoinTeamPresenceFactor { get; init; } = 0.98; // expected decrease: 3x

		/// <summary>
		/// Simple brute-force protection: only allow teams to submit
		/// a small number of solutions for a given function, then lock them out.
		/// </summary>
		public int MaxSolutionSubmitAttempts { get; init; } = 5;

		/// <summary>
		/// How much can the submitted solution and the real solution differ at most to still be accepted.
		/// </summary>
		public double MaxResultAttemptDifference { get; init; } = 0.005;

		public string AdminPassword { get; init; } = "kryptohroch";
	}
}
