#!/usr/bin/env python3

import urllib.parse
import urllib.request

savepath = "../BitcoinGame/wwwroot/images/"
format = "png"

def get_image(tex, uid):
	url = "https://latex.codecogs.com/" + format + ".download?\dpi{150}" + urllib.parse.quote(tex)
	filename = savepath + str(uid) + "." + format
	urllib.request.urlretrieve(url, filename)

def main():
	with open("funkce.csv") as f:
		f.readline() # header line
		id = 0
		for line in f:
			splits = line.split(";")
			uid = splits[0]
			tex = splits[1]
			get_image(tex, uid)
			id += 1


if __name__ == '__main__':
	main()
