#!/usr/bin/env python3

import random
import math
import cmath
import scipy.special
# random.randint includes both bounds

class Func:
	def __init__(self, tex, results, stars):
		self.tex = tex
		self.results = results
		self.stars = stars
		self.ordering = stars

class Frac:
	def __init__(self, num, den):
		self.num = num
		self.den = den
	
	@classmethod
	def new_random(cls, max_num, max_den):
		den = random.randint(2, max_den)
		num = den
		while euclid(num, den) != 1:
			num = random.randint(2, max_num)
		return cls(num, den)
	
	def __str__(self):
		sign = "" if self.num > 0 else "-"
		return sign + "\\tfrac{" + str(abs(self.num)) + "}{" + str(self.den) + "}"

	def __float__(self):
		return self.num / self.den

seed_step = 1000
num_in_group = 5

funcs = []

def add(tex, results, stars):
	funcs.append(Func(tex, results, stars))

def postprocess(tex):
	tex = tex.replace("++", "+")
	tex = tex.replace("+-", "-")
	if tex.startswith(" "):
		tex = tex[1:]
	if tex.startswith("+"):
		tex = tex[1:]
	return tex

def print_funcs(reward):
	random.seed(1)
	for f in funcs:
		f.ordering = f.stars + (random.random() - 0.5) * 1.6

	funcs_sorted = sorted(funcs, key=lambda x: x.ordering)
	# funcs_sorted = funcs

	for f in funcs_sorted:
		print(";".join([hex(abs(hash(f.tex)))[2:], postprocess(f.tex), ",".join(map(str, f.results)), str(f.stars), str(reward)]))


def rand_elem(arr):
	return arr[random.randint(0, len(arr) - 1)]

def rand_sign(pos_prob):
	return 1 if random.random() < pos_prob else -1

def euclid(a, b):
	if a < b:
		a, b = b, a
	while b > 0:
		a, b = b, a % b
	return a

def degrees(rad):
	return 180 * rad / math.pi

def positive_period(deg):
	if deg < 0:
		deg += 360
	return deg

def eq(a, b):
	eps = 10e-10
	return - eps < a - b and a - b < eps

def term(x, power):
	unknown = "x^" + str(power)
	if power == 1:
		unknown = "x"
	if power == 0:
		unknown = ""

	x = int(round(x))
	if x == 0:
		return ""
	elif abs(x) == 1:
		sign = "+" if x > 0 else "-"
		return f"{sign}{unknown}"
	else:
		return f"{x}{unknown}"

# * generic function builders

def quadratic(a, x1, x2, stars):
	b = - a * (x1 + x2)
	c = a * x1 * x2
	tex = f"{term(a, 2)}+{term(b, 1)}+{term(c, 0)} = 0"
	add(tex, [x1, x2], stars)

def cubic(a, x1, x2, x3, stars):
	b = - a * (x1 + x2 + x3)
	c = a * (x1 * x2 + x2 * x3 + x3 * x1)
	d = - a * x1 * x2 * x3
	tex = f"{term(a.real, 3)}+{term(b.real, 2)}+{term(c.real, 1)}+{term(d.real, 0)} = 0"
	add(tex, [x1], stars)

def quartic(a, x1, x2, x3, x4, stars):
	b = - a * (x1 + x2 + x3 + x4)
	c = a * (x1 * x2 + x2 * x3 + x3 * x4 + x4 * x1 + x1 * x3 + x2 * x4)
	d = - a * (x2 * x3 * x4 + x1 * x3 * x4 + x1 * x2 * x4 + x1 * x2 * x3)
	e = a * x1 * x2 * x3 * x4
	tex = f"{term(a.real, 4)}+{term(b.real, 3)}+{term(c.real, 2)}+{term(d.real, 1)}+{term(e.real, 0)} = 0"
	add(tex, [x1, x2], stars)

# * function sets

def example_linear():
	for i in range(10):
		random.seed(i + 1 * seed_step)
		value = rand_sign(0.5) * random.randint(1, 20)
		tex = "x+" + str(value) + " = 0"
		add(tex, [-value], 1)

def example_quadratic():
	for i in range(10):
		random.seed(i + 2 * seed_step)
		value = random.randint(5, 20)
		tex = "x^2-" + str(value) + " = 0"
		add(tex, [ math.sqrt(value) ], 2)

def with_fractions():
	for i in range(num_in_group * 6):
		random.seed(i + 3 * seed_step)
		num_terms = random.randint(3, 6)
		max_den = math.floor(8 / math.log2(num_terms))
		sum_x = 0
		sum_const = 0
		tex = ""
		for j in range(num_terms):
			use_x = (eq(sum_x, 0) and j == num_terms - 1) or random.random() < 0.3
			value = 0
			# integer
			if random.random() < 0.4:
				value = rand_sign(0.5) * random.randint(1, 10)
				tex += "+" + str(value)
			# fraction
			else:
				frac = Frac.new_random(10, max_den)
				tex += "+" + str(frac)
				value = float(frac)
			
			if use_x:
				sum_x += value
				tex += "x"
			else:
				sum_const += value
		
		tex += " = 0"
		resuls = [ - sum_const / sum_x ]
		add(tex, resuls, 1)

def quadratic_with_integer_solutions():
	for i in range(num_in_group * 2):
		random.seed(i + 4 * seed_step)
		x1 = rand_sign(0.5) * random.randint(1, 20)
		x2 = rand_sign(0.5) * random.randint(1, 20)
		a = rand_sign(0.75) * random.randint(1, 5)
		diff = abs(x1) + abs(x2)
		quadratic(a, x1, x2, 1)

def quadratic_with_rational_solutions():
	for i in range(num_in_group * 4):
		random.seed(i + 5 * seed_step)
		x1 = rand_sign(0.5) * random.randint(1, 20)
		x2 = rand_sign(0.5) * random.randint(1, 20)
		a = rand_sign(0.75) * random.randint(1, 5)
		div1 = random.randint(2,5)
		div2 = random.randint(2,5)
		diff = abs(x1) + abs(x2) + abs(div1) + abs(div2)
		quadratic(a * div1 * div2, x1 / div1, x2 / div2, 1)

def quadratic_with_irational_solutions():
	for i in range(num_in_group * 10):
		random.seed(i + 6 * seed_step)
		roots = [2,3,5,6,7,8]
		disc = math.sqrt(rand_elem(roots))
		b = rand_sign(0.5) * random.randint(1, 15)
		a = rand_sign(0.75) * random.randint(1, 5)
		div = random.randint(1, 3)
		diff = abs(b) + 2 * div
		quadratic(a * div * div, (b + disc) / div, (b - disc) / div, 2)

def cubic_with_integer_solutions():
	# uses two complex roots
	for i in range(num_in_group * 3):
		random.seed(i + 7 * seed_step)
		x1 = rand_sign(0.5) * random.randint(1, 10)
		x2 = rand_sign(0.5) * random.randint(1, 3) + random.randint(1, 3) * 1j
		x3 = x2.conjugate()
		a = rand_sign(0.75) * random.randint(1, 5)
		diff = abs(x1) + abs(x2.real)
		cubic(a, x1, x2, x3, 1)

def quartic_with_integer_solutions():
	# uses two complex roots
	for i in range(num_in_group * 3):
		random.seed(i + 8 * seed_step)
		x1 = rand_sign(0.5) * random.randint(1, 5)
		x2 = rand_sign(0.5) * random.randint(1, 10)
		x3 = rand_sign(0.5) * random.randint(1, 3) + random.randint(1, 3) * 1j
		x4 = x3.conjugate()
		a = rand_sign(0.75) * random.randint(1, 5)
		diff = abs(x1) + abs(x2) + abs(x3.real)
		quartic(a, x1, x2, x3, x4, 1)

def quartic_with_irrational_solutions():
	# two multiplied quadratics
	for i in range(num_in_group * 20):
		random.seed(i + 9 * seed_step)
		roots = [2,3,5,6,7,8]
		disc1 = math.sqrt(rand_elem(roots))
		b1 = rand_sign(0.5) * random.randint(1, 15)
		x3 = rand_sign(0.5) * random.randint(1, 3) + random.randint(1, 3) * 1j
		x4 = x3.conjugate()
		a = rand_sign(0.75) * random.randint(1, 5)
		div = random.randint(1, 3)
		quartic(a * div * div, (b1 + disc1) / div, (b1 - disc1) / div, x3, x4, 4)

def powers_of_integers():
	for i in range(num_in_group * 5):
		random.seed(i + 10 * seed_step)
		power = Frac.new_random(8, 10)
		
		diff = power.den + max([power.num, power.den])
		min_inter_result = max(2, 15 - diff)
		inter_result = random.randint(min_inter_result, min_inter_result + 4)
		tex = "x^{" + str(power) + "} = " + str(inter_result ** power.num)
		add(tex, [inter_result ** power.den], 1)

def squares():
	for i in range(num_in_group * 3):
		random.seed(i + 11 * seed_step)
		base = random.randint(20, 99) + 0.1 + 0.8 * random.random()
		squared = round(base**2)
		base = math.sqrt(squared)
		tex = "x^2=" + str(int(squared))
		add(tex, [base], 2)

def cubes():
	for i in range(num_in_group * 3):
		random.seed(i + 12 * seed_step)
		base = random.randint(10, 25) + 0.1 + 0.8 * random.random()
		cubed = round(base**3)
		base = math.pow(cubed, 1/3)
		tex = "x^3=" + str(int(cubed))
		add(tex, [base], 2)
		
# ! May cause a division by zero exception. Fix by changing the rng seed ^.^
def exponential_with_common_base():
	for i in range(num_in_group * 4):
		random.seed(i + 13 * seed_step + 40)
		base = rand_elem([2, 3, 5])
		power1 = random.randint(1, 5)
		power2 = random.randint(power1 + 1, 8)

		x_coeff1 = rand_sign(0.5) * random.randint(1, 10)
		x_coeff2 = rand_sign(0.5) * random.randint(1, 10)
		const_coeff1 = rand_sign(0.5) * random.randint(1, 10)
		const_coeff2 = rand_sign(0.5) * random.randint(1, 10)

		tex = f"{base**power1}^{{ {x_coeff1}x+{const_coeff1} }} = {base**power2}^{{ {x_coeff2}x+{const_coeff2} }}"
		x_sum = power1 * x_coeff1 - power2 * x_coeff2
		const_sum = power1 * const_coeff1 - power2 * const_coeff2
		results = [ - const_sum / x_sum ]
		add(tex, results, 1)

def exponential_with_different_bases():
	for i in range(num_in_group * 10):
		random.seed(i + 14 * seed_step)
		bases = Frac.new_random(100, 100) # ugly code reuse
		base1 = bases.num
		base2 = bases.den
		base_factor = math.log2(base1) / math.log2(base2)

		x_coeff1 = rand_sign(0.5) * random.randint(1, 10)
		x_coeff2 = rand_sign(0.5) * random.randint(1, 10)
		const_coeff1 = rand_sign(0.5) * random.randint(1, 10)
		const_coeff2 = rand_sign(0.5) * random.randint(1, 10)

		tex = f"{base1}^{{ {x_coeff1}x+{const_coeff1} }} = {base2}^{{ {x_coeff2}x+{const_coeff2} }}"
		x_sum = base_factor * x_coeff1 - x_coeff2
		const_sum = base_factor * const_coeff1 - const_coeff2
		results = [ - const_sum / x_sum ]
		add(tex, results, 3)

def sines():
	for i in range(num_in_group * 3):
		random.seed(i + 15 * seed_step)
		frac = Frac.new_random(30, 50)
		while float(frac) >= 1:
			frac = Frac.new_random(30, 50)
		frac.num *= rand_sign(0.5)

		tex = "\sin x = " + str(frac)
		sol = degrees(math.asin(float(frac)))
		add(tex, [positive_period(sol), positive_period(180 - sol)], 2)

def cosines():
	for i in range(num_in_group * 3):
		random.seed(i + 16 * seed_step)
		frac = Frac.new_random(30, 50)
		while float(frac) >= 1:
			frac = Frac.new_random(30, 50)
		frac.num *= rand_sign(0.5)

		tex = "\cos x = " + str(frac)
		sol = degrees(abs(math.acos(float(frac)))) # abs works because cos is even
		add(tex, [sol, 360 - sol], 2)

def lambert():
	for i in range(num_in_group * 10):
		random.seed(i + 17 * seed_step)
		value = random.randint(20, 1000)
		base = random.randint(2, 9)
		sol = scipy.special.lambertw(value * math.log(base)) / math.log(base)
		tex = f"x {base}^x = {value}"
		add(tex, [sol.real], 3)

def xlog():
	for i in range(num_in_group * 10):
		random.seed(i + 18 * seed_step)
		value = random.randint(20, 1000)
		base = random.randint(2, 9)
		power = random.randint(2, 9)
		sol = math.e**(scipy.special.lambertw(power * value * math.log(base)) / power)
		tex = f"x^{power} log_{base} x = {value}"
		add(tex, [sol.real], 3)

# * main

def main():
	print("hash;tex;results;stars;reward")

	# example_linear()
	# example_quadratic()

	# print_funcs(1)
	# funcs = []

	with_fractions()
	quadratic_with_integer_solutions()
	quadratic_with_rational_solutions()
	quadratic_with_irational_solutions()
	cubic_with_integer_solutions()
	quartic_with_integer_solutions()
	quartic_with_irrational_solutions()
	powers_of_integers()
	squares()
	cubes()
	exponential_with_common_base()
	exponential_with_different_bases()
	sines()
	cosines()
	lambert()
	xlog()

	print_funcs(10)

if __name__ == '__main__':
	main()
